import { deepMerge, filterKeys, frontmatter, nano, path } from './deps.ts';

type Template = {
	nodes: ReturnType<typeof nano.parse>;
	// deno-lint-ignore no-explicit-any
	front_matter: Record<string, any>;
};

// deno-lint-ignore no-explicit-any
export type Filter = (...args: any[]) => unknown;

function baseData(input: string): {
	layout?: string;
	title?: string;
	contents: string;
} {
	if (frontmatter.test(input)) {
		const matter = frontmatter.extract<{
			layout: string;
		}>(input);

		return {
			...matter.attrs,
			contents: matter.body,
		};
	} else {
		return {
			contents: input,
		};
	}
}

export interface EngineOptions {
	/**
	 * Cache the parsed templates on the first request, and
	 * stringify them on request.
	 *
	 * Only applicable to {@link Engine.renderFile}.
	 *
	 * Note that in development, this will prevent you from
	 * changing the file while the server is running.
	 *
	 * @default false
	 */
	cache?: boolean;
}

/**
 * Wrapper for Nano templating engine.
 * Adds frontmatter, layouts, caching, and render-by-file.
 *
 * @see https://deno.land/x/nano
 */
class Engine {
	#options: Required<EngineOptions>;
	#cache: Map<string, Template>;
	#root: string;
	#filters: Record<string, Filter> = {};
	// deno-lint-ignore no-explicit-any
	#data: Record<string, any> = {};

	/**
	 * @param root Directory with the template files
	 */
	constructor(root: string, options?: EngineOptions) {
		this.#cache = new Map();
		this.#root = root;

		this.#options = {
			cache: false,
			...(options ?? {}),
		};
	}

	/** A list of all defined filters */
	get filters() {
		return { ...this.#filters };
	}

	/** Add a filter */
	addFilter(name: string, fn: Filter) {
		this.#filters[name] = fn;
	}

	/** Global data */
	get data() {
		return { ...this.#data };
	}

	/** Add global data */
	addData(key: string, value: unknown) {
		this.#data[key] = value;
	}

	// deno-lint-ignore no-explicit-any
	async render(input: string, data: Record<string, any> = {}): Promise<string> {
		data = deepMerge(this.data, data);

		const { contents, title, ...base } = baseData(input);
		data = deepMerge(data, base);

		// Compile "title" frontmatter key
		if (title) {
			const _data = filterKeys(data, k => !['layout', 'title'].includes(k));
			data.title = await this.render(title, _data);
		}

		const page = await nano.render(contents, data, this.#filters, {
			import_directory: this.#root,
		});

		if (data.layout) {
			return this.renderFile(data.layout, {
				...data,
				contents: page,
			});
		}

		return page;
	}

	async renderFile(
		filepath: string,
		// deno-lint-ignore no-explicit-any
		data: Record<string, any> = {},
	): Promise<string> {
		let tpl: Template;

		if (this.#cache.has(filepath)) {
			tpl = this.#cache.get(filepath)!;
		} else {
			const input = await Deno.readTextFile(
				path.resolve(this.#root, filepath),
			);

			const { contents, ...base } = baseData(input);

			tpl = {
				nodes: nano.parse(nano.scan(contents)),
				front_matter: base,
			};

			if (this.#options.cache) this.#cache.set(filepath, tpl);
		}

		data = deepMerge(this.data, data);
		data = deepMerge(data, tpl.front_matter);

		// Compile "title" frontmatter key
		if (data.title) {
			const _data = filterKeys(data, k => !['layout', 'title'].includes(k));
			data.title = await this.render(data.title, _data);
		}

		const page = await nano.compile(tpl.nodes, data, this.#filters, {
			import_directory: path.dirname(
				path.resolve(this.#root, filepath),
			),
		});

		// If this file was just used as a layout, return now.
		// Otherwise, we'll loop endlessly.
		if (data.layout === filepath) {
			return page;
		}

		if (data.layout) {
			return this.renderFile(data.layout, {
				...data,
				contents: page,
			});
		}

		return page;
	}

	clearCache(filepath?: string) {
		if (filepath) {
			return this.#cache.delete(filepath);
		}

		this.#cache.clear();
	}
}

export { Engine };
