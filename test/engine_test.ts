import { assertEquals } from 'https://deno.land/std@0.160.0/testing/asserts.ts';
import { path } from '../deps.ts';

import { Engine } from '../engine.ts';

Deno.test('Engine render properly', async (t) => {
	const engine = new Engine(
		path.dirname(path.fromFileUrl(import.meta.url)),
	);

	await t.step('renders local variable', async () => {
		const actual = await engine.render('Hi, my name is {{ name }}.', {
			name: 'Binyamin',
		});

		assertEquals(actual, 'Hi, my name is Binyamin.');
	});

	await t.step('renders global variable', async () => {
		engine.addData('name', 'Binyamin');
		const actual = await engine.render('Hi, my name is {{ name }}.');
		assertEquals(actual, 'Hi, my name is Binyamin.');
	});

	await t.step('renders with a filter', async () => {
		engine.addFilter('lowercase', (str: string) => str.toLowerCase());

		const actual = await engine.render('{{ var | lowercase }}', {
			var: 'UPPER',
		});

		assertEquals(actual, 'upper');
	});

	await t.step('parses frontmatter', async () => {
		const actual = await engine.render(
			'---\nname: Binyamin\n---\nHello {{ name }}',
		);

		assertEquals(actual, 'Hello Binyamin');
	});

	await t.step('Compiles "title" key in frontmatter', async (t) => {
		await t.step('Using supplied data', async () => {
			const actual = await engine.render(
				'---\ntitle: "Hello, {{ name }}!"\n---\n{{ title }}',
				{ name: 'Binyamin' },
			);

			assertEquals(actual, 'Hello, Binyamin!');
		});

		await t.step('Using frontmatter', async () => {
			const actual = await engine.render(
				'---\nname: Bob\ntitle: "Hello, {{ name }}!"\n---\n{{ title }}',
			);

			assertEquals(actual, 'Hello, Bob!');
		});

		await t.step('Using supplied data & renderFile', async () => {
			const actual = await engine.renderFile('template-title_test.html', {
				name: 'Bob',
			});

			assertEquals(actual, 'Hello, Bob!');
		});
	});

	await t.step('layouts work', async () => {
		const actual = await engine.render('Lorem ipsum', {
			layout: 'layout_test.html',
		});
		assertEquals(actual, `<p>Lorem ipsum</p>`);
	});

	await t.step('dependent layouts work', async () => {
		const actual = await engine.render('Lorem ipsum', {
			layout: 'layout-2_test.html',
		});

		assertEquals(actual, `<p><i>Lorem ipsum</i></p>`);
	});
});
