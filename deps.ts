export * as path from 'https://deno.land/std@0.160.0/path/mod.ts';
export * as frontmatter from 'https://deno.land/std@0.160.0/encoding/front_matter.ts';
export { deepMerge } from 'https://deno.land/std@0.160.0/collections/deep_merge.ts';
export { filterKeys } from 'https://deno.land/std@0.160.0/collections/filter_keys.ts';

export * as nano from 'https://deno.land/x/nano@v0.1.2/mod.ts';

declare module 'https://deno.land/x/oak@v11.1.0/mod.ts' {
	interface Context {
		// deno-lint-ignore no-explicit-any
		render: (filename: string, data?: Record<string, any>) => Promise<void>;
	}
}

export * as oak from 'https://deno.land/x/oak@v11.1.0/mod.ts';
