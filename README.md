# Oak Nano Views

Template-rendering middleware for [oak](https://github.com/oakserver/oak), using
the [nano](https://github.com/alevroub/nano) templating engine.

Compatible with Deno Deploy.

## Usage

The module's default export is a function which returns middleware.

```js
// Import the middleware
import views from 'https://denopkg.dev/gl/binyamin/oak-nano-views/mod.ts';

// Initialize oak
const app = new oak.Application();

// Initialize the middleware with the folder containing your
// templates.
app.use(views('views'));
```

### Global Data

Any data stored under `app.state.globals` is passed to templates as global data.

```js
// Initialize oak
const app = new oak.Application();

// Define global data under `app.state.globals`.
app.state.globals = {
	title: 'My Awesome Website',
};
```

### Render Templates

Use the `context.render` method

```js
// Initialize an oak router
const router = new oak.Router();

router.get(async (ctx, next) => {
	await ctx.render('index.html', {
		// Add data just for this template. (optional)
	});
	await next();
});
```

Templates may use frontmatter. To use another file as a layout, set the `layout`
front-matter key to its relative path. The template's contents will then be
available under the `contents` variable.

## Contributing

All input is welcome; feel free to
[open an issue](https://gitlab.com/binyamin/oak-nano-views/-/issues/new). Please
remember to be a [mensch](https://www.merriam-webster.com/dictionary/mensch). If
you want to program, you can browse
[the issue list](https://gitlab.com/binyamin/oak-nano-views/-/issues).

## Legal

All source-code is provided under the terms of
[the MIT license](https://gitlab.com/binyamin/oak-nano-views/-/blob/main/LICENSE).
Copyright 2022 Binyamin Aron Green.
