import { deepMerge, oak, path } from './deps.ts';
import { Engine, type EngineOptions, type Filter } from './engine.ts';

interface Options extends EngineOptions {
	filters?: Record<string, Filter>;
}

/**
 * @param root Folder with templates/views
 */
function views(root: string, options: Options = {}): oak.Middleware {
	const engine = new Engine(path.resolve(root), options);

	if (options.filters) {
		for (const [name, fn] of Object.entries(options.filters)) {
			engine.addFilter(name, fn);
		}
	}

	return async (ctx, next) => {
		// deno-lint-ignore no-explicit-any
		ctx.render = async (file: string, data: Record<string, any> = {}) => {
			if (typeof ctx.state?.globals === 'object') {
				data = deepMerge(ctx.state.globals, data);
			}

			try {
				const contents = await engine.renderFile(file, data);

				ctx.response.body = contents;
				// TODO calculate based on file extension (foo.njk vs. foo.xml.njk);
				ctx.response.headers.set('Content-Type', 'text/html; charset=utf-8');
			} catch (error) {
				const status = oak.Status.InternalServerError;

				if (error instanceof Deno.errors.NotFound) {
					ctx.throw(status, 'The server tried to render a non-existent file.', {
						cause: error,
					});
				} else if (error instanceof Error) {
					ctx.throw(status, error.message, {
						cause: error,
					});
				} else {
					ctx.throw(status, error, {
						cause: error,
					});
				}
			}
		};

		await next();
	};
}

export default views;
